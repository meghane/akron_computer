# Akron Computer Repair Website

## About

This is the website for the Akron Computer Repair Service Co.

## Technology
Based off Bootstrap Rapid Template, this should only include client-side code. For now,
there will be no server-side processing.

## Getting Started
  + Clone the project down using [git](https://git-scm.com/downloads)
```

git clone git@bitbucket.org:meghane/akron_computer.git

```

  + Open the project with [Atom](https://atom.io/) or your favorite text editor.


## Adding SSH Keys for git:
[SSH Agent Instructions](https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

## Git Usage
```

git status
git add <filename>   ## add specific file
git add -A   ## add all files which have been changed
git commit -m 'change history message'
git push origin master

```
